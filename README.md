# spring security-learn

#### 介绍
spring security学习demo

#### 软件架构
springboot+mybatis plus+spring security+java jwt

#### 安装教程
每个子工程的application.yml中配置数据库信息，端口统一8080，若要同时运行子项目就需要改成不同端口

#### 使用说明
1. 子工程simple简单实现登录功能，用户名和密码储存在内存中
2. 子工程basic是基本功能，即实现从数据库登录
3. 子工程token实现了jwt的登录验证，禁用了session
1、2、3都只涉及验证，不涉及授权
