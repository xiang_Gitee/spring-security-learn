package com.controller;

import com.auth0.jwt.JWT;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;

@RestController
public class HelloController {
    

    @GetMapping("/hello")
    public String hello() {
        return "登陆成功";
    }

    @GetMapping("/home")
    public String home() {
        return "首页成功，未登录噢";
    }

    @GetMapping("/signin")
    public String login() {
        return "请先登录";
    }

    @GetMapping("/error")
    public String error() {

        return "登录失败";
    }
}
